# README #

This is sample code for tektalk 11: Socket.IO

### Hướng dẫn cài đặt ###

* Download & Install NodeJS: http://nodejs.org
* Mở terminal, cd đến thư mục workspace (thư mục chứa file server.js). 
* Cài modules trong package.json bằng câu lệnh: 'npm install' (gõ trên terminal).
* Sau khi cài đặt xong, mở cổng app bằng câu lệnh 'node server'
* Vào class ClientConfig.swift, chỉnh IP thành localhost (test trên simulator) hoặc thay thành ip của laptop đang kết nối cùng mạng với real device.