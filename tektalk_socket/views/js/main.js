/* user object */
var client		= {
	client_id: '',
	socket_id: ''
};

/* handle socket events */
var Events 		= {
	onConnected: function (returnClient) {
		client.client_id = returnClient.name;
		client.socket_id = returnClient.socket;
	},

	msg: function (msgContent) {
		console.log(msgContent);
	},
};

/* socket object */
var socket		= io.connect('localhost:2015/ios');
socket.on('onConnected', Events.onConnected);
socket.on('msg', Events.msg);
