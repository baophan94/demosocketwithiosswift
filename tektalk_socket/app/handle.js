module.exports = function (io, Stream, fs) {
	
	var ios = io.of('/ios');
	ios.on('connection', function(client){
		console.log('someone connected');
		client.join('myiOSGroup');
		ios.to('myiOSGroup').emit('msg', 'blah');
	});

	io.on('connection', function (client) {
		
		/*
			█▀▀ █▀▀█ █▀▀▄ █▀▀▄ █▀▀ █▀▀ ▀▀█▀▀ █▀▀ █▀▀▄ 
			█░░ █░░█ █░░█ █░░█ █▀▀ █░░ ░░█░░ █▀▀ █░░█ 
			▀▀▀ ▀▀▀▀ ▀░░▀ ▀░░▀ ▀▀▀ ▀▀▀ ░░▀░░ ▀▀▀ ▀▀▀░ 
		*/

		console.log(client.id);

		console.log('New client connected!');
		Stream.add(getRandomNumber(), client.id);
		console.log('Number of clients: ' + Stream.numberOfUsers());
		client.emit('onConnected', Stream.getBySocket(client.id));


		/*
			█▀▀ █▀▀ █▀▀▄ █▀▀▄ 　 █▀▄▀█ █▀▀ █▀▀ █▀▀ █▀▀█ █▀▀▀ █▀▀ 
			▀▀█ █▀▀ █░░█ █░░█ 　 █░▀░█ █▀▀ ▀▀█ ▀▀█ █▄▄█ █░▀█ █▀▀ 
			▀▀▀ ▀▀▀ ▀░░▀ ▀▀▀░ 　 ▀░░░▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀░░▀ ▀▀▀▀ ▀▀▀ 
			@param messageDetails: 	- to: receiver socket id
									- message: message text to send
		*/
		client.on('sendMessage', function (messageDetails) {
			console.log('sendMessage: ' + JSON.stringify(messageDetails));
			var receiver = io.sockets.connected[messageDetails.to];
			if (receiver) {
				receiver.emit('onMessage', messageDetails.message);
			}
		});

		client.on('sendFile', function (fileDetail) {
			console.log(fileDetail);
			var fileName = '123.png';
			console.log(fileDetail);
			fs.writeFile(
				 'views/img/' + fileName,
				fileDetail,
				function (error) { 
					if (error) { 
						console.log(error);
						return; 
					} 
					console.log('OK');
				});
		});


		/*
			█▀▀ █▀▀ ▀▀█▀▀ 　 █▀▀ ▀▀█▀▀ █▀▀█ ▀▀█▀▀ █░░█ █▀▀ 
			▀▀█ █▀▀ ░░█░░ 　 ▀▀█ ░░█░░ █▄▄█ ░░█░░ █░░█ ▀▀█ 
			▀▀▀ ▀▀▀ ░░▀░░ 　 ▀▀▀ ░░▀░░ ▀░░▀ ░░▀░░ ░▀▀▀ ▀▀▀
			@param isChatting (boolean type)
		*/
		client.on('setStatus', function (isChatting) {
			Stream.setStatus(client.id, isChatting);
		});


		/*
			█▀▀▀ █▀▀ ▀▀█▀▀ 　 █▀▀ ▀▀█▀▀ █▀▀█ ▀▀█▀▀ █░░█ █▀▀ 
			█░▀█ █▀▀ ░░█░░ 　 ▀▀█ ░░█░░ █▄▄█ ░░█░░ █░░█ ▀▀█ 
			▀▀▀▀ ▀▀▀ ░░▀░░ 　 ▀▀▀ ░░▀░░ ▀░░▀ ░░▀░░ ░▀▀▀ ▀▀▀ 
			@param remoteSocket remote socket id need to get status
			@return isChat (boolean type)
		*/
		client.on('getStatus', function (remoteSocket) {
			client.emit('onGotStatus', Stream.getStatus(remoteSocket));
		});


		/*
			█▀▀▀ █▀▀ ▀▀█▀▀ 　 █▀▀ █░░ ░▀░ █▀▀ █▀▀▄ ▀▀█▀▀ █▀▀ 
			█░▀█ █▀▀ ░░█░░ 　 █░░ █░░ ▀█▀ █▀▀ █░░█ ░░█░░ ▀▀█ 
			▀▀▀▀ ▀▀▀ ░░▀░░ 　 ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀░░▀ ░░▀░░ ▀▀▀ 
			@return allStream (JSONArray -> JSONObjects: name, socket, isChat)
		*/
		client.on('getClients', function () {
			client.emit('onGotClients', Stream.getAllStreams());
		});

		/*
			█▀▀▄ ░▀░ █▀▀ █▀▀ █▀▀█ █▀▀▄ █▀▀▄ █▀▀ █▀▀ ▀▀█▀▀ █▀▀ █▀▀▄ 
			█░░█ ▀█▀ ▀▀█ █░░ █░░█ █░░█ █░░█ █▀▀ █░░ ░░█░░ █▀▀ █░░█ 
			▀▀▀░ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀▀ ▀░░▀ ▀░░▀ ▀▀▀ ▀▀▀ ░░▀░░ ▀▀▀ ▀▀▀░ 
		*/
		client.on('disconnect', function () {
			console.log('Client was disconnected!');
			Stream.removeBySocket(client.id);
			console.log('Number of clients: ' + Stream.numberOfUsers());
		});

	});

	function getRandomNumber () {
		return Math.floor(Math.random() * 9000) + 1000;
	};

	/*
	function decodeBase64Image(dataString) {
		var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
		response = {};
		if (matches.length !== 3) {
			return new Error('Invalid input string');
		}
		response.type = matches[1];
		response.data = new Buffer(matches[2], 'base64');
		return response;
	};
	*/

};