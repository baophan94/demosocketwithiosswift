

module.exports = function () {

	/* store all online clients */
	var allStreams = [];

	/* client object */
	var ClientObject = function (name, socket) {
		this.name 	= name;
		this.socket	= socket;
		this.isChat = false;
	};

	return {

		/* add new client to list */
		add: function (name, socket) {
			var stream = new ClientObject(name, socket);
			allStreams.push(stream);
		},

		/* get stream object by socket */
		getBySocket: function (socket) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].socket != socket) {
				index++;
			}
			if (!allStreams[index]) {
				var empty = new ClientObject('', '');
				return empty;
			}
			return allStreams[index];
		},

		/* get stream object by name */
		getByName: function (name) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].name != name) {
				index++;
			}
			if (!allStreams[index]) {
				var empty = new ClientObject('', '');
				return empty;
			}
			return allStreams[index];
		},

		/* get number of users online */
		numberOfUsers: function () {
			return allStreams.length;
		},

		/* check stream name is exist */
		isStreamExist: function (name) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].name != name) {
				index++;
			}
			if (!allStreams[index]) {
				return false;
			}
			return true;
		},

		/* set chatting status */
		setStatus: function (socket, isChat) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].socket != socket) {
				index++;
			}
			if (!allStreams[index]) {
				return;
			}
			allStreams[index].isChat = isChat;
		},

		/* get chatting status */
		getStatus: function (socket) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].socket != socket) {
				index++;
			}
			if (!allStreams[index]) {
				return true;
			}
			return allStreams[index].isChat;
		},

		/* get client name by socket */
		getName: function (socket) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].socket != socket) {
				index++;
			}
			if (!allStreams[index]) {
				return '';
			}
			return allStreams[index].name;
		},

		/* get socket by client name */
		getSocket: function (name) {
			var index = 0;
			while(index < allStreams.length && allStreams[index].name != name) {
				index++;
			}
			if (!allStreams[index]) {
				return '';
			}
			return allStreams[index].socket;
		},

		/* get streams by know first index and number of streams need to get */
		getStreams: function (firstIndex, numberOfItems) {
			if (!allStreams[firstIndex]) {
				return [];
			}
			var index = firstIndex + numberOfItems;
			if (index > (allStreams.length - 1)) {
				index = (allStreams.length - 1);
			}
			var items = [];
			for (firstIndex; firstIndex <= index; firstIndex++) {
				var item = allStreams[firstIndex];
				if (item) {
					items.push(item);
				}
			}
			return items;
		},

		/* get all client object from list */
		getAllStreams: function () {
			return allStreams;
		},

		/* change client name by know socket */
		editName: function (socket, name) {
			var client = allStreams.find(function(element, i, array) {
				return element.socket == socket;
			});
			client.name = name;
		},

		/* change device socket id by know name */
		editSocket: function (name, socket) {
			var client = allStreams.find(function(element, i, array) {
				return element.name == name;
			});
			client.socket = socket;
		},

		/* remove client by name */
		removeByName: function (name) {
			var index = 0;
			while (index < allStreams.length && allStreams[index].name != name) {
				index++;
			}
			allStreams.splice(index, 1);
		},

		/* remove client by socket */
		removeBySocket: function (socket) {
			var index = 0;
			while (index < allStreams.length && allStreams[index].socket != socket) {
				index++;
			}
			allStreams.splice(index, 1);
		}

	};

};