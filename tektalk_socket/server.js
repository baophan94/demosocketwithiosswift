/* port */
var DEFAULT_PORT	= 2015;

/* import module express */
var express			= require('express');
var app				= express();
var fs				= require('fs');

/* config app */
app.set('port', DEFAULT_PORT);
app.get('/', function (request, response) {
	response.charset = 'UTF-8';
	response.sendFile(__dirname + '/public/index.html');
});
app.use('/', express.static(__dirname + '/views'));

/* start server */
var server = app.listen(app.get('port'), function () {
	console.log('Server started at port ' + app.get('port'));
});

/* import socket module */
var socket			= require('socket.io').listen(server, {'log': false});

/* handle socket connection */
var stream 			= require('./app/stream.js')();
require('./app/handle.js')(socket, stream, fs);
