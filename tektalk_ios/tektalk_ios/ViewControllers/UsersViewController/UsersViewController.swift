//
//  UsersViewController.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class UsersViewController: UITableViewController {
	
	var allUsers: NSMutableArray!

	// MARK: Life Cycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		initComponents()
		customizeViews()
		initSocket()
		
    }
	

    // MARK: Process Methods
	
	func initComponents() {
		self.tableView.registerNib(
			UINib(nibName: UserTableViewCell.identifier, bundle: nil),
			forCellReuseIdentifier: UserTableViewCell.identifier)
		allUsers = NSMutableArray()
		
		let infoBtn = UIBarButtonItem(
			barButtonSystemItem: .Add,
			target: self,
			action: "showSelfInfo")
		self.navigationItem.rightBarButtonItem = infoBtn
		
		let refreshButton = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "RefreshUserList")
		self.navigationItem.leftBarButtonItem = refreshButton
	}
	
	func RefreshUserList() {
		AppDelegate.socket.emit(Events.getClients, []);
	}
	
	func customizeViews() {
		title = "TekTalk Socket.IO"
		self.clearsSelectionOnViewWillAppear = false
	}
	
	func initSocket() {
		
		/* this will received all events from server - FOR TESTING ONLYYYY */
		/*
		AppDelegate.socket.onAny {
			print("Got event: \($0.event), with items: \($0.items)")
		}
		*/
		
		handleSocketEvents()
		
		/* start connect to server via socket connection */
		AppDelegate.socket.connect()
	}
	
	func handleSocketEvents() {
		
		// Call when client was connected to server
		// @return JSON object
		AppDelegate.socket.on(Events.onConnected) { (data, ack) -> Void in
			let clientObject = data[0]
			ClientConfig.client.name	= String(clientObject["name"] as! Int)
			ClientConfig.client.socket	= clientObject["socket"] as? String
			ClientConfig.client.isChat	= false
			AppDelegate.socket.emit(Events.getClients, [])
		}
		
		// Received list of users online
		// @return JSON array
		AppDelegate.socket.on(Events.onGotClients) { (data, ack) -> Void in
			let clientObject = data[0] as! NSArray
			self.allUsers.removeAllObjects()
			for (_, element) in EnumerateSequence(clientObject) {
				let item	= Client()
				item.name	= String(element["name"] as! Int)
				item.socket = element["socket"] as? String
				item.isChat = element["isChat"] as! Bool
				self.allUsers.addObject(item)
			}
			self.tableView.reloadData()
		}
		
	}
	
	/* Show current client information */
	func showSelfInfo() {
		let message = "Your id: \(ClientConfig.client.name!)\nSocket id: \(ClientConfig.client.socket!)"
		showAlert("Your Info", message: message)
	}
	
	func showAlert(title: String, message: String) {
		let infoAlert = UIAlertController(
			title: title,
			message: message,
			preferredStyle: UIAlertControllerStyle.Alert)
		infoAlert.addAction(UIAlertAction(
			title: "Close",
			style: .Default,
			handler: nil))
		
		self.presentViewController(
			infoAlert, animated: false, completion: nil)
	}
	
	

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView,
							numberOfRowsInSection section: Int) -> Int {
        return allUsers.count
    }

    override func tableView(
		tableView: UITableView,
		cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
			
		let cell:UserTableViewCell = (tableView.dequeueReusableCellWithIdentifier(
			UserTableViewCell.identifier,
			forIndexPath: indexPath) as? UserTableViewCell)!
			
		let item: Client = (allUsers[indexPath.row] as? Client)!
		cell.lbUserId.text = item.name
		if !item.isChat {
			cell.imgStatus.image = UIImage(named: "available")
		}
		else {
			cell.imgStatus.image = UIImage(named: "busy")
		}
			
        return cell
    }
	
	
	
	// MARK: - Table view delegate
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let item: Client = (allUsers[indexPath.row] as? Client)!
		/*
		if item.socket == ClientConfig.client.socket {
			self.showAlert("!!! Error !!!", message: "Tự kỷ hả má 😱")
			return
		}
		*/
		ClientConfig.partner = item
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let chatScreen = storyboard.instantiateViewControllerWithIdentifier(ChatScreenViewController.identifier)
		self.navigationController?.pushViewController(
			chatScreen, animated: true)
	}
	

}
