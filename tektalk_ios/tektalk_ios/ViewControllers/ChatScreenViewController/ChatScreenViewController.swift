//
//  ChatScreenViewController.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class ChatScreenViewController: UIViewController,
								UITextFieldDelegate,
								UITableViewDataSource,
								UITableViewDelegate {
	
	static let identifier = "ChatScreenViewController"
	
	var allMessages: NSMutableArray!
	@IBOutlet weak var conversation: UITableView!
	@IBOutlet weak var bottomSpace: NSLayoutConstraint!
	@IBOutlet weak var txtMessage: UITextField!
	// MARK: Life Cycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		initComponents()
		customizeViews()
		duLieuGia()
		addSocketHandler();
		
    }
	
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		let image	= UIImage(named: "avatar")
		let data		= UIImageJPEGRepresentation(image!, 1.0)
		
		AppDelegate.socket.emit("sendFile", data!);
		
	}
	
	
	// MARK: User Actions
	
	@IBAction func sendMessage(sender: AnyObject) {
		emitMessage(txtMessage.text!)
	}
	
	
	
	// MARK: Process Methods
	
	func duLieuGia() {
		/*
			let item1 = Message(sender: "1234", message: "Hajimemashite! Watashi ha YaBao desu. Yoroshiku~", isSelf: true)
			allMessages.addObject(item1)
			let item2 = Message(sender: "2323", message: "Watto desu! Kochirakoso yoroshiku~", isSelf: false)
			allMessages.addObject(item2)
			let item3 = Message(sender: "1234", message: "Kono chatto ha nan desu ka?", isSelf: true)
			allMessages.addObject(item3)
			let item4 = Message(sender: "2323", messaghie: "Sokketo (Socket) no apuri desu ne~", isSelf: false)
			allMessages.addObject(item4)
			let item5 = Message(sender: "1234", message: "ooooooo sugoiiiii! desu neeee", isSelf: true)
			allMessages.addObject(item5)
		*/
	}
	
	func customizeViews() {
		conversation.separatorStyle		= .None
		conversation.estimatedRowHeight = 44.0
		conversation.rowHeight			= UITableViewAutomaticDimension
	}
	
	func initComponents() {
		allMessages = NSMutableArray()
		txtMessage.delegate = self
		conversation.registerNib(
			UINib(nibName: IncommingTableViewCell.identifier, bundle: nil),
			forCellReuseIdentifier: IncommingTableViewCell.identifier)
		conversation.registerNib(
			UINib(nibName: SelfChatTableViewCell.identifier, bundle: nil),
			forCellReuseIdentifier: SelfChatTableViewCell.identifier)
		conversation.dataSource = self
		conversation.delegate	= self
	}
	
	func addSocketHandler() {
		AppDelegate.socket.on(Events.onMessage) { (data, ack) -> Void in
			let messageObject = data[0] as! String
			let msg = Message(
				sender: ClientConfig.client.name!,
				message: messageObject, isSelf: false)
			self.allMessages.addObject(msg)
			self.conversation.reloadData()
			self.scrollToBottom()
		}
	}
	
	func emitMessage(message: String) {
		txtMessage.endEditing(true)
		if message == "" {
			return
		}
		let payload = [
			"to": ClientConfig.partner.socket!,
			"message": message
		]
		AppDelegate.socket.emit(Events.sendMessage, payload)
		let msg = Message(
			sender: ClientConfig.client.name!,
			message: message, isSelf: true)
		allMessages.addObject(msg)
		conversation.reloadData()
		txtMessage.text = ""
	}
	
	override func touchesBegan(
		touches: Set<UITouch>,
		withEvent event: UIEvent?) {
		txtMessage.endEditing(true)
	}
	
	func scrollToBottom() {
		let indexPath = NSIndexPath(
			forRow: self.allMessages.count - 1,
			inSection: 0)
		self.conversation.selectRowAtIndexPath(indexPath,
			animated: true, scrollPosition: .None)
	}
	
	
	
	// MARK: Handle Protocol Methods

	func textFieldDidBeginEditing(textField: UITextField) {
		bottomSpace.constant = AppConfig.KEYBOARD_HEIGHT_PORTRAIT
		UIView.animateWithDuration(0.3,
			animations: { () -> Void in
				self.view.layoutIfNeeded()
			}) { (finished) -> Void in
				self.scrollToBottom()
		}
	}
	
	func textFieldDidEndEditing(textField: UITextField) {
		bottomSpace.constant = 0
		UIView.animateWithDuration(0.3) { () -> Void in
			self.view.layoutIfNeeded()
		}
	}
	
	
	// MARK: TableView Datasource
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView,
		numberOfRowsInSection section: Int) -> Int {
		return allMessages.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let message: Message = (allMessages.objectAtIndex(indexPath.row) as? Message)!
		
		/* Current client message */
		if message.isSelf {
			let cell: SelfChatTableViewCell = (tableView.dequeueReusableCellWithIdentifier(SelfChatTableViewCell.identifier, forIndexPath: indexPath) as? SelfChatTableViewCell)!
			cell.txtMsg.text = message.message
			return cell
		}
			
		/* Partner message */
		else {
			let cell: IncommingTableViewCell = (tableView.dequeueReusableCellWithIdentifier(IncommingTableViewCell.identifier, forIndexPath: indexPath) as? IncommingTableViewCell)!
			cell.txtMsg.text = message.message
			return cell
		}
			
	}
	
}
