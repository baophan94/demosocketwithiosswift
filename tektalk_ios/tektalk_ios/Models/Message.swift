//
//  Message.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class Message: NSObject {
	
	var sender		= ""
	var message		= ""
	var isSelf		= false
	
	init(sender: String,
		 message: String,
		 isSelf: Bool) {
		self.sender		= sender
		self.message	= message
		self.isSelf		= isSelf
	}
	
}
