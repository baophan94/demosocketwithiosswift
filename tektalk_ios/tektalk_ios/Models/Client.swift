//
//  Client.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class Client: NSObject {
	var name: String?
	var socket: String?
	var isChat: Bool
	
	override init() {
		name	= ""
		socket	= ""
		isChat	= false
	}
	
}
