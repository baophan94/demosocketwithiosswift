//
//  UserTableViewCell.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
	
	internal static let identifier = "UserTableViewCell"

	@IBOutlet weak var lbUserId: UILabel!
	@IBOutlet weak var imgStatus: UIImageView!
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
		selectionStyle = .None;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

		
    }
    
}
