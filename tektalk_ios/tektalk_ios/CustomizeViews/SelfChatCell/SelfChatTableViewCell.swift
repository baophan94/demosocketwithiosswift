//
//  SelfChatTableViewCell.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class SelfChatTableViewCell: UITableViewCell {

	internal static let identifier = "SelfChatTableViewCell"
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var txtMsg: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		selectionStyle = .None;
	}
	
	override func layoutSubviews() {
		containerView.clipsToBounds			= true
		containerView.layer.cornerRadius	= 5.0
	}
	
	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
