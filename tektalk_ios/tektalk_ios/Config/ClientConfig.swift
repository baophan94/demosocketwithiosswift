//
//  ClientConfig.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class ClientConfig: NSObject {
	
	// Server address
	static let HOST_ADDRESS = "192.168.1.63:2015"
	
	// Self information
	static var client		= Client()
	
	// Receiver information (in conversation)
	static var partner		= Client()
	
}
