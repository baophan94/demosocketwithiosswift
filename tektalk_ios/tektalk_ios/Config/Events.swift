//
//  Events.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class Events: NSObject {
	
	// Handle from server
	static let onConnected	= "onConnected"
	static let onGotClients	= "onGotClients"
	static let onMessage	= "onMessage"
	
	// Emit to server
	static let getClients	= "getClients"
	static let sendMessage	= "sendMessage"
	
}
