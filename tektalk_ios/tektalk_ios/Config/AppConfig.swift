//
//  AppConfig.swift
//  tektalk_ios
//
//  Created by baophan on 2015/09/25.
//  Copyright © 2015年 @baophan94. All rights reserved.
//

import UIKit

class AppConfig: NSObject {

	static let KEYBOARD_HEIGHT_PORTRAIT: CGFloat	= 216.0
	static let KEYBOARD_HEIGHT_LANDSCAPE: CGFloat	= 162.0
	
}
